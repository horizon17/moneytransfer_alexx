DROP TABLE IF EXISTS Account;

CREATE TABLE Account (AccountId LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
Balance DECIMAL(15,2), Currency VARCHAR(20));

INSERT INTO Account (Balance,Currency) VALUES (1000.00,'usd');
INSERT INTO Account (Balance,Currency) VALUES (2000.00,'usd');
INSERT INTO Account (Balance,Currency) VALUES (3000.00,'usd');
INSERT INTO Account (Balance,Currency) VALUES (4000.00,'eur');
INSERT INTO Account (Balance,Currency) VALUES (5000.00,'eur');
