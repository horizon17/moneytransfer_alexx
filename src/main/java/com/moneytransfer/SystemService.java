package com.moneytransfer;

import com.moneytransfer.utils.Utils;
import org.apache.commons.dbutils.DbUtils;
import org.h2.tools.RunScript;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * H2
 */
public class SystemService {

    private static final String h2_driver = Utils.getStringProperty("h2_driver");
    private static final String h2_conn_url = Utils.getStringProperty("h2_connection_url");
    private static final String h2_user = Utils.getStringProperty("h2_user");
    private static final String h2_password = Utils.getStringProperty("h2_password");


    SystemService() {
        DbUtils.loadDriver(h2_driver);
    }

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(h2_conn_url, h2_user, h2_password);

    }

    public void setData() {

        Connection conn = null;
        try {
            conn = getConnection();
            RunScript.execute(conn, new FileReader("src/main/resources/data.sql"));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeQuietly(conn);
        }
    }

    public static ResultSet getResultSet(String query,
                                         List param) throws Exception {

        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = SystemService.getConnection();
            stmt = conn.prepareStatement(query);
            for (Object next : param) {
                stmt.setObject(1, next);
            }
            return stmt.executeQuery();
        } catch (Exception e) {
            throw new Exception();
        }
    }

}
