package com.moneytransfer;

import com.moneytransfer.service.AccountController;
import com.moneytransfer.service.AccountService;
import com.moneytransfer.utils.Utils;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;


/**
 * Application.
 */
public class Application {

    private static final int port = Utils.getIntegerProperty("server.port", 80);

    public static void main(String[] args) throws Exception {

        SystemService systemService = new SystemService();
        systemService.setData();

        startService();

    }

    private static void startService() throws Exception {
        Server server = new Server(port);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);
        ServletHolder servletHolder = context.addServlet(ServletContainer.class, "/*");
        servletHolder.setInitParameter("jersey.config.server.provider.classnames",
                AccountController.class.getCanonicalName()+ ","
                + AccountService.class.getCanonicalName());
        try {
            server.start();
            server.join();
        } finally {
            server.destroy();
        }
    }



}
