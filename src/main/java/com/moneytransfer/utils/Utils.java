package com.moneytransfer.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class Utils {

    private static Properties properties = new Properties();

    public static void loadConfig(String fileName) {

        try {

            final InputStream fis = Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
            properties.load(fis);
        } catch (IOException ioe) {

        }

    }

    public static String getStringProperty(String key) {
        String value = properties.getProperty(key);
        if (value == null) {
            value = System.getProperty(key);
        }
        return value;
    }

    /**
     * @param key:       property key
     * @param defaultVal the default value if the key not present in config file
     * @return string property based on lookup key
     */
    public static int getIntegerProperty(String key, int defaultVal) {
        String valueStr = getStringProperty(key);
        if (valueStr == null) {
            return defaultVal;
        } else {
            try {
                return Integer.parseInt(valueStr);

            } catch (Exception e) {
                return defaultVal;
            }
        }
    }

    static {
        String configFileName = System.getProperty("application.properties");

        if (configFileName == null) {
            configFileName = "application.properties";
        }
        loadConfig(configFileName);

    }


}
