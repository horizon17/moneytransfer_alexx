package com.moneytransfer.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransferDto {

    @JsonProperty(required = true)
    private long accountIdFrom;

    @JsonProperty(required = true)
    private long accountIdTo;

    @JsonProperty(required = true)
    private BigDecimal amount;

}
