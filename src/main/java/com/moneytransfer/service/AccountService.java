package com.moneytransfer.service;

import com.moneytransfer.SystemService;
import com.moneytransfer.dto.Account;
import com.moneytransfer.dto.TransferDto;
import org.apache.commons.dbutils.DbUtils;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Account Service
 */
public class AccountService {

    private final static String GET_ACC_BY_ID = "SELECT * FROM Account WHERE AccountId = ? ";
    private final static String UPDATE_ACC_BALANCE = "UPDATE Account SET Balance = ? WHERE AccountId = ? ";
    private final static String GET_ALL_ACC = "SELECT * FROM Account";
    private final static String LOCK_ACC_BY_ID = "SELECT * FROM Account WHERE AccountId = ? FOR UPDATE";

    public List<Account> getAllAccounts() throws Exception {

        ResultSet rs = SystemService.getResultSet(GET_ALL_ACC, new ArrayList());
        List<Account> allAccounts = new ArrayList<Account>();
        while (rs.next()) {
            Account acc = new Account(rs.getLong("AccountId"),
                    rs.getBigDecimal("Balance"),
                    rs.getString("Currency"));
            allAccounts.add(acc);
        }
        return allAccounts;
    }

    public Account getAccountById(long accountId) throws Exception {
        List<Object> list = new ArrayList<>();
        list.add(accountId);
        ResultSet rs;

        rs = SystemService.getResultSet(GET_ACC_BY_ID, list);

        Account acc = null;
        if (rs.next()) {
            acc = new Account(rs.getLong("AccountId"),
                    rs.getBigDecimal("Balance"),
                    rs.getString("Currency"));
        }
        return acc;
    }

    public String doTranfer(TransferDto transferDto) throws Exception {

        Account accountFrom = getAccountById(transferDto.getAccountIdFrom());
        Account accountTo = getAccountById(transferDto.getAccountIdTo());
        if (!accountFrom.getCurrency().equals(accountTo.getCurrency())) {
            return "Currency not match";
        }

        BigDecimal amount = transferDto.getAmount();
        String result = updateAccountBalance(transferDto.getAccountIdFrom(), amount.negate());
        if ("success".equals(result)) {
            updateAccountBalance(transferDto.getAccountIdTo(), amount);
            return "";
        }
        return result;
    }

    /**
     * Изменить баланс.
     */
    public String updateAccountBalance(long accountId,
                                       BigDecimal deltaAmount) throws Exception {

        Account targetAccount = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        PreparedStatement update = null;
        ResultSet rs = null;
        try {
            conn = SystemService.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.prepareStatement(LOCK_ACC_BY_ID);
            stmt.setObject(1, accountId);
            rs = stmt.executeQuery();

            if (rs.next()) {
                targetAccount = new Account(rs.getLong("AccountId"),
                        rs.getBigDecimal("Balance"),
                        rs.getString("Currency"));
            }

            if (targetAccount == null) {
                return "Account not found";
            }

            BigDecimal balance = targetAccount.getBalance().add(deltaAmount);
            if (balance.compareTo(new BigDecimal(0)) < 0) {
                return "not enough money";
            }
            update = conn.prepareStatement(UPDATE_ACC_BALANCE);
            update.setBigDecimal(1, balance);
            update.setLong(2, accountId);
            update.executeUpdate();
            conn.commit();
        } catch (Exception e) {
            throw new Exception();
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(stmt);
            DbUtils.closeQuietly(update);
        }

        return "success";

    }

}
