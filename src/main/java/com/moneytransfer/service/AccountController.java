package com.moneytransfer.service;


import com.moneytransfer.Description;
import com.moneytransfer.Skip;
import com.moneytransfer.dto.Account;
import com.moneytransfer.dto.TransferDto;
import com.moneytransfer.utils.Utils;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.List;

/**
 * Account Controller
 */
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class AccountController {

    private AccountService accountService = new AccountService();

    /**
     * Все ендпойнты.
     *
     * @return
     * @throws
     */
    @Skip
    @GET
    @Path("/start")
    public String getAllEndpoints() throws Exception {

        AnnotationAnalyzer analyzer = new AnnotationAnalyzer();
        List<String> list = analyzer.analyz(AccountController.class);
        String result = "";
        for (String str : list) {
            result = result + str + "\n";
        }
        return result;

    }


    @Description(text = "money transfer between accounts", method = "POST",
                example = "/transfer; body:{\"accountIdFrom\":1,\"accountIdTo\":2,\"amount\":100}")
    @POST
    @Path("/transfer")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response doTransfer(TransferDto transferDto) throws Exception {

        String result = accountService.doTranfer(transferDto);

        if (!result.equals("")) {
            return Response.status(Response.Status.PRECONDITION_FAILED).entity(result).build();
        }
        Account account = accountService.getAccountById(transferDto.getAccountIdTo());

        return Response.ok("Balance for accountId = " + transferDto.getAccountIdTo()
                + " is " + account.getBalance() + " " + account.getCurrency()).build();

    }

    @Description(text = "get accounts list", method = "GET", example = "/all")
    @GET
    @Path("/all")
    public List<Account> getAllAccounts() throws Exception {

        return accountService.getAllAccounts();

    }

    @Description(text = "Find by account id", method = "GET", example = "/1")
    @GET
    @Path("/{accountId}")
    public Account getAccountById(@PathParam("accountId") long accountId) throws Exception {

        return accountService.getAccountById(accountId);

    }


    @Description(text = "Find balance by account Id", method = "GET", example = "/balance/1")
    @GET
    @Path("/balance/{accountId}")
    public BigDecimal getBalance(@PathParam("accountId") long accountId) throws Exception {
        Account account = accountService.getAccountById(accountId);

        if (account == null) {
            throw new WebApplicationException("Account not found", Response.Status.NOT_FOUND);
        }
        return account.getBalance();

    }

}
