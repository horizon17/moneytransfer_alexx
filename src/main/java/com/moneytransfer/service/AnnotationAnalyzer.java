package com.moneytransfer.service;

import com.moneytransfer.Description;
import com.moneytransfer.Skip;

import javax.ws.rs.Path;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class AnnotationAnalyzer {

    public List analyz(Class<?> clazz) throws Exception {
        Method[] methods = clazz.getMethods();

        List<String> list = new ArrayList<>();

        for (Method method : methods) {
            String result = "";
            if (method.isAnnotationPresent(Skip.class)) {
                continue;
            }
            if (method.isAnnotationPresent(Path.class)) {

                Path test = method.getAnnotation(Path.class);
                result = test.value();

            }
            if (method.isAnnotationPresent(Description.class)) {

                Description description = method.getAnnotation(Description.class);
                result = result + ", " + description.text() + ", " + description.method() + ", example: " + description.example();

            }
            list.add(result);
        }
        return list;
    }

}
