package com.moneytransfer;

import com.moneytransfer.dto.Account;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ControllerTest extends TestService {

    @Test
    public void testTransfer() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/transfer").build();
        HttpPost post = new HttpPost(uri);
        StringEntity params =new StringEntity("{\"accountIdFrom\":1,\"accountIdTo\":2,\"amount\":100}");
        post.addHeader("content-type", "application/json");
        post.setEntity(params);
        HttpResponse response = client.execute(post);
        int statusCode = response.getStatusLine().getStatusCode();
        assertEquals(200, statusCode);

        String balance = EntityUtils.toString(response.getEntity());
        assertEquals("Balance for accountId = 2 is 2100.00 usd", balance);
    }

    @Test
    public void testGetBalance() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/balance/1").build();
        HttpGet request = new HttpGet(uri);
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertEquals(200, statusCode);

        String balance = EntityUtils.toString(response.getEntity());
        assertEquals(new BigDecimal("1000.00"), new BigDecimal(balance));
    }

    @Test
    public void testGetAll() throws IOException, URISyntaxException {
        URI uri = builder.setPath("/all").build();
        HttpGet request = new HttpGet(uri);
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        assertEquals(200, statusCode);

        String jsonString = EntityUtils.toString(response.getEntity());
        Account[] accounts = mapper.readValue(jsonString, Account[].class);
        assertTrue(accounts.length == 5);

    }

    @Test
    public void testGetAccountById() throws IOException, URISyntaxException {

        URI uri = builder.setPath("/1").build();
        HttpGet request = new HttpGet(uri);
        HttpResponse response = client.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();

        assertEquals(200, statusCode);

        String jsonString = EntityUtils.toString(response.getEntity());
        Account account = mapper.readValue(jsonString, Account.class);
        assertTrue(account.getCurrency().equals("usd"));

    }



}
